﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Les1
{
    class ActionMultiply : IMenuAction
    {

        public string Title()
        {
            return "Multiply";
        }

        public void Run()
        {
            Console.Write("Enter FIRST number: ");
            double a = Input.ReadInt();
            Console.Write("Enter SECOND number: ");
            double b = Input.ReadInt();
            Console.WriteLine("\nYour result: {0} * {1} = {2}", a, b, a * b);
        }
    }
}
