﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Les1
{
    class HTMLLogger : ILogger
    {
        private FileStream f;
        private StreamWriter writer;

        public void OpenFile(string path)
        {
            f = new FileStream(path, FileMode.Create, FileAccess.Write);
            writer = new StreamWriter(f);
            writer.Write("<html>");
            writer.Write("<body>");
        }

        public void CloseFile()
        {
            writer.Write("</body>");
            writer.Write("</html>");
            writer.Close();
        }

        public void OnWrite(string s)
        {
            string text = "<p>" + s + "</p>";
            writer.WriteLine(text);
            writer.Flush();
        }
    }
}
