﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Les1
{
    class Log
    {
        public static Log inst = new Log();

        delegate void SaveMsg(string s);
        SaveMsg handler;

        public static void RegOutput(ILogger l)
        {
            inst.handler += l.OnWrite;
        }

        public static void Write(string s)
        {
            inst.handler.Invoke(s);
        }

    }
}
