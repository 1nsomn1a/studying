﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Les1
{

    class Program
    {
        static void Main(string[] args)
        {
            TextLogger tl = new TextLogger();
            tl.OpenFile("log.txt");

            HTMLLogger hl = new HTMLLogger();
            hl.OpenFile("log1.html");

            Log.RegOutput(tl);
            Log.RegOutput(hl);
            Log.Write("Start");

            Menu mainmenu = new Menu();
            mainmenu.AddAction(new ActionSumm());
            mainmenu.AddAction(new ActionMinus());
            mainmenu.AddAction(new ActionMultiply());
            mainmenu.AddAction(new ActionDivide());

            ActionFinder find = new ActionFinder();
            find.SetSource(mainmenu.actions);
            mainmenu.AddAction(find);
            mainmenu.exec();

            Log.Write("End");
            hl.CloseFile();
        }
    }
}
