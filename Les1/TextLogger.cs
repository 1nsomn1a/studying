﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Les1
{
    class TextLogger : ILogger
    {
        private FileStream f;
        private StreamWriter writer;

        public void OpenFile(string path)
        {
            f = new FileStream(path, FileMode.Append, FileAccess.Write);
            writer = new StreamWriter(f);
        }



        public void OnWrite(string s)
        {
            string text = DateTime.Now.ToString() + " " + s;
            writer.WriteLine(text);
            writer.Flush();
        }
    }
}
