﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Les1
{
    class ActionFinder : IMenuAction
    {
        List<IMenuAction> actions = new List<IMenuAction>();


        public void SetSource(List<IMenuAction> a)
        {
            actions = a;
        }
        public string Title()
        {
            return "Vvod deystviya";
        }

        public void Run()
        {
            Console.WriteLine("Enter the action name");
            string name = Console.ReadLine();
            bool flag = false;
            for (int i = 0; i < actions.Count; i++)
            {
                if (name == actions[i].Title())
                {
                    actions[i].Run();
                    flag = true;
                }
            }
            if (!flag)
            {
                Console.WriteLine("Incorrect name action");
            }
        }
    }
}
