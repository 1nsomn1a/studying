﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Les1
{
    interface IMenuAction
    {
        string Title();
        void Run();
    }
}
