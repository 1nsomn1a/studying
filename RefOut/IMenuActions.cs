﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RefOut
{
    interface IMenuAction
    {
        string Title();
        void Run();
    }
}
