﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RefOut
{
    class Action
    {
        internal static double c;
        internal static void Summ(double a, double b)
        {
            c = (double)(a + b);
            Console.WriteLine("Your result: " + c);
        }

        internal static void Substruct(double a, double b, out double c)
        {
            c = (double)(a - b);
            Console.WriteLine("Your result: " + c);
        }

        internal static void Multiply(double a, double b, out double c)
        {
            c = (double)(a * b);
            Console.WriteLine("Your result: " + c);
        }

        internal static void Divide(double a, double b, out double c)
        {
            c = (double)(a / b);
            Console.WriteLine("Your result: " + c);
        }

    }
}
