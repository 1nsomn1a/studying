﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RefOut
{
    class ActionDiv : Action, IMenuAction
    {
        public string Title()
        {
            return "Divide";
        }

        public void Run()
        {
            Console.Write("Enter FIRST number: ");
            double a = Input.Check();
            Console.Write("Enter SECOND number: ");
            double b = Input.Check();
            Action.Divide(a, b, out c);
        }
    }
}