﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RefOut
{
    class ActionSum : Action, IMenuAction
    {
        public string Title()
        {
            return "Summa";
        }

        public void Run()
        {
            Console.Write("Enter FIRST number: ");
            double a = Input.Check();
            Console.Write("Enter SECOND number: ");
            double b = Input.Check();
            Action.Summ(a, b);
        }
    }
}