﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RefOut
{
    public static class Input
    {
        public static double Check()
        {
            for (; ;)
            {
                try
                {
                    double a = Convert.ToDouble(Console.ReadLine());
                    return a;
                }
                catch (FormatException)
                {
                    Console.Write("Incorrect format. Please try again: ");
                }
            }
        }
    }
}
