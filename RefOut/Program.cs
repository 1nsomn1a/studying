﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RefOut
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu mainmenu = new Menu();
            mainmenu.AddAction(new ActionSum());
            mainmenu.AddAction(new ActionSub());
            mainmenu.AddAction(new ActionMul());
            mainmenu.AddAction(new ActionDiv());

            mainmenu.exec();

        }
    }
}
