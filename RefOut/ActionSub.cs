﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RefOut
{
    class ActionSub : Action, IMenuAction
    {
        public string Title()
        {
            return "Substruct";
        }

        public void Run()
        {
            Console.Write("Enter FIRST number: ");
            double a = Input.Check();
            Console.Write("Enter SECOND number: ");
            double b = Input.Check();
            Action.Substruct(a, b, out c);
        }
    }
}
