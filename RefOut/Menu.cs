﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RefOut
{
    class Menu
    {
        List<IMenuAction> actions = new List<IMenuAction>();
        public void AddAction(IMenuAction a)
        {
            actions.Add(a);
        }

        public void ShowMenu()
        {
            Console.Clear();
            for (int i = 0; i < actions.Count; i++)
            {
                Console.WriteLine("{0}. {1}", i+1, actions[i].Title());
                if (i == actions.Count-1)
                {
                    Console.WriteLine("0. Exit");
                }
            }        
        }

        public void exec()
        {
            for (; ; )
            {
            link:
                ShowMenu();
                int index = Console.ReadKey().KeyChar -'0';
                if ((index < 0) || (index > actions.Count))
                {
                    goto link;
                }
                if (index == 0)
                {
                    break;
                }
                IMenuAction m = actions[index - 1];
                Console.Clear();
                Console.WriteLine("Your choise is ***{0}***\n", m.Title());
                m.Run();
                Console.WriteLine("\n\nPress any key to return into main menu...");
                Console.ReadKey();
            }
        }
    }
}
