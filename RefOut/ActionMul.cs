﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RefOut
{
    class ActionMul : Action, IMenuAction
    {
        public string Title()
        {
            return "Multiply";
        }

        public void Run()
        {
            Console.Write("Enter FIRST number: ");
            double a = Input.Check();
            Console.Write("Enter SECOND number: ");
            double b = Input.Check();
            Action.Multiply(a, b, out c);
        }
    }
}